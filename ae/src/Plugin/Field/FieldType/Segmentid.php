<?php

/**
 *
 * @author Avery K.
 *
 * Contains field type "ae_segmentid".
 *
 * @FieldType(
 *   id = "ae_segmentid",
 *   label = @Translation("AE Segment ID"),
 *   description = @Translation("AE SegmentID field."),
 *   category = @Translation("AE"),
 *   default_widget = "ae_segmentid_default",
 *   default_formatter = "ae_segmentid_default",
 * )
 *
 * Code snippits taken from :
 * https://capgemini.github.io/drupal/writing-custom-fields-in-drupal-8/
 */


 namespace Drupal\ae\Plugin\Field\FieldType;
  use Drupal\Core\TypedData\DataDefinition;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Field\FieldItemInterface;
  use Drupal\Core\Field\FieldStorageDefinitionInterface;
  use Drupal\Core\Field\FieldItemBase;

class Segmentid extends FieldItemBase implements FieldItemInterface {

    const SEGMENTID_MAX_LENGTH = 32;

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {

        return array(
          'columns' => array(
            'id' => array(
              'type' => 'int',
              'length' => static::SEGMENTID_MAX_LENGTH,
              'not null' => FALSE,
            ),
          ),
          'indexes' => array(
            'id' => array('id'),
          ),
        );

    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

        $properties['id'] = DataDefinition::create('integer')
          ->setLabel(t('Segmentid'))->setRequired(FALSE);

        return $properties;

    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {

        $id = $this->get('id')->getValue();
        return $id === NULL || $id === '';

    }

    /**
     * {@inheritdoc}
     */
    public function getConstraints() {

        $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
        $constraints = parent::getConstraints();
        $constraints[] = $constraint_manager->create('ComplexData', array(
         'id' => array(
           'Length' => array(
             'max' => static::SEGMENTID_MAX_LENGTH,
             'maxMessage' =>
                t('%name: the segment ID cannot exceed @max digits.',
                array('%name' => $this->getFieldDefinition()->getLabel(),
                '@max' => static::SEGMENTID_MAX_LENGTH)),
           )
         ),
        ));

        return $constraints;

    }

}
