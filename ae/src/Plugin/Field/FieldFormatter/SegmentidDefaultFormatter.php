<?php

/**
 * @author Avery K.
 * Contains field type "ae_segmentid".
 *
 * Field widget "ae_segmentid_default".
 *
 * @FieldFormatter(
 *   id = "ae_segmentid_default",
 *   label = @Translation("SegmentID default"),
 *   field_types = {
 *     "ae_segmentid",
 *   }
 * )
 *
 * Code snippits taken from : https://evolvingweb.ca/fr/blog/burrito-maker-how-create-custom-fields-drupal-8
 */

 namespace Drupal\ae\Plugin\Field\FieldFormatter;
 use Drupal\Core\Field\FieldItemListInterface;
 use Drupal\Core\Field\FormatterBase;
 use Drupal;

class SegmentidDefaultFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {

        $elements = array();

        foreach ($items as $delta => $item) {

            $elements[$delta] = array('#markup' => '');

        }

        return $elements;

    }

}
