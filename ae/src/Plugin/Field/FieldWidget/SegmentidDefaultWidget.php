<?php

/**
 * @author Avery K.
 * Contains field type "ae_segmentid".
 *
 * Field widget "ae_segmentid_default".
 *
 * @FieldWidget(
 *   id = "ae_segmentid_default",
 *   label = @Translation("SegmentID default"),
 *   field_types = {
 *     "ae_segmentid",
 *   }
 * )
 *
 * Code snippits taken from : https://capgemini.github.io/drupal/writing-custom-fields-in-drupal-8/
 */

namespace Drupal\ae\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

class SegmentidDefaultWidget extends WidgetBase {

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

        $element['id'] = $element + array(
            '#type' => 'number',
            '#empty_value' => '',
            '#default_value' => (isset($items[$delta]->id)) ? $items[$delta]->id : NULL,
            '#description' => t('AE SegmentID'),
          );
          
        return $element;

    }

}
