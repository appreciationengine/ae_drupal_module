<?php

namespace Drupal\ae\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'AE Window Sign On' Block.
 *
 * @Block(
 *   id = "ae_window",
 *   admin_label = @Translation("AE Window"),
 *   category = @Translation("AE Sign On Links"),
 * )
 */
class AeWindowBlock extends BlockBase implements BlockPluginInterface {

    private $text;
    private $type;
    private $return;
    private $show_after_login;

    public function __construct()
    {
        $this->state = \Drupal::state();
    }

    /**
     * {@inheritdoc}
     */
    public function build() {

        $this->setBlockProps();

        return [
            '#theme' => 'aewindowblock',
            '#text' => $this->text,
            '#type' => $this->type,
            '#return' => $this->return,
            '#show_after_login' => $this->show_after_login
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {

        $form = parent::blockForm($form, $form_state);
        $config = $this->getConfiguration();

        $form['type_options'] = array(
            '#type' => 'value',
            '#value' => array(
                'register' => t('register'),
                'login' => t('login'),
            ),
        );

        $form['ae_window_block']['type'] = array(
          '#title' => t('Sign On Type'),
          '#type' => 'select',
          '#description' => 'Choose register to allow users to signup and login.
          Choose login if you only want your users to be able to login through this link',
          '#options' => $form['type_options']['#value'],
          '#default_value' => isset($config['ae_window_block']['type']) ? $config['ae_window_block']['type'] : 'register',
        );

        $form['ae_window_block']['text'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Link Text'),
          '#description' => $this->t('The text that will be displayed for this sign on link'),
          '#default_value' => isset($config['ae_window_block']['text']) ? $config['ae_window_block']['text'] : '',
        );

        $form['ae_window_block']['return'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Return URL'),
          '#description' => $this->t('The URL destination you would like to send your users after sign in'),
          '#default_value' => isset($config['ae_window_block']['return']) ? $config['ae_window_block']['return'] : '',
        );

        $form['ae_window_block']['show_after_login'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Show this link after login'),
          '#description' => $this->t('Do you want this sign on link to be visible after login?'),
          '#default_value' => isset($config['ae_window_block']['show_after_login']) ? $config['ae_window_block']['show_after_login'] : '',
        );

        return $form;

    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        parent::blockSubmit($form, $form_state);
        $this->configuration['ae_window_block'] = $form_state->getValue('ae_window_block');
        ksm($this->configuration['ae_window_block']);
    }

    public function setBlockProps() {
        $config = $this->getConfiguration();
        $window_block_config = $config['ae_window_block'];

        $this->text = $window_block_config['text'] ? $window_block_config['text'] : '';
        $this->type =  $window_block_config['type'] ? $window_block_config['type'] : 'register';
        $this->return = $window_block_config['return'] ? $window_block_config['return'] : '';
        $this->show_after_login = $window_block_config['show_after_login'] == 1 ? true : false;

    }

}
?>
