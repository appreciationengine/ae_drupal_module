<?php

namespace Drupal\ae\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'AE Window Sign On' Block.
 *
 * @Block(
 *   id = "ae_link",
 *   admin_label = @Translation("AE Link"),
 *   category = @Translation("AE Sign On Links"),
 * )
 */
class AeLinkBlock extends BlockBase implements BlockPluginInterface {

    private $text;
    private $service;
    private $type;
    private $return;
    private $show_after_login;

    public function __construct()
    {
        $this->state = \Drupal::state();
    }

    /**
     * {@inheritdoc}
     */
    public function build() {

        $this->setBlockProps();

        return [
            '#theme' => 'aelinkblock',
            '#text' => $this->text,
            '#service' => $this->service,
            '#type' => $this->type,
            '#return' => $this->return,
            '#show_after_login' => $this->show_after_login
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {

        $form = parent::blockForm($form, $form_state);
        $config = $this->getConfiguration();

        $form['type_options'] = array(
            '#type' => 'value',
            '#value' => array(
                'register' => t('register'),
                'login' => t('login'),
            ),
        );

        $form['service_options'] = $this->getSelectedSocialsOptions();

        $form['ae_link_block']['service'] = array(
          '#title' => t('Service Type'),
          '#type' => 'select',
          '#description' => 'Which social service would you like your users to sign in? Make sure you have enabled
          services in the AE Configuration Settings -> General -> Socials',
          '#options' => $form['service_options']['#value'],
          '#default_value' => isset($config['ae_link_block']['service']) ? $config['ae_link_block']['service'] : 'register',
        );

        $form['ae_link_block']['type'] = array(
          '#title' => t('Sign On Type'),
          '#type' => 'select',
          '#description' => 'Choose register to allow users to signup and login.
          Choose login if you only want your users to be able to login through this link',
          '#options' => $form['type_options']['#value'],
          '#default_value' => isset($config['ae_link_block']['type']) ? $config['ae_link_block']['type'] : 'register',
        );

        $form['ae_link_block']['text'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Link Text'),
          '#description' => $this->t('The text that will be displayed for this sign on link'),
          '#default_value' => isset($config['ae_link_block']['text']) ? $config['ae_link_block']['text'] : '',
        );

        $form['ae_link_block']['return'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Return URL'),
          '#description' => $this->t('The URL destination you would like to send your users after sign in'),
          '#default_value' => isset($config['ae_link_block']['return']) ? $config['ae_link_block']['return'] : '',
        );

        $form['ae_link_block']['show_after_login'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Show this link after login'),
          '#description' => $this->t('Do you want this sign on link to be visible after login?'),
          '#default_value' => isset($config['ae_link_block']['show_after_login']) ? $config['ae_link_block']['show_after_login'] : '',
        );

        return $form;

    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        parent::blockSubmit($form, $form_state);
        $this->configuration['ae_link_block'] = $form_state->getValue('ae_link_block');
        ksm($this->configuration['ae_link_block']);
    }

    public function setBlockProps() {
        $config = $this->getConfiguration();
        $window_block_config = $config['ae_link_block'];

        $this->text = $window_block_config['text'] ? $window_block_config['text'] : '';
        $this->service = $window_block_config['service'] ? $window_block_config['service'] : 'email';
        $this->type =  $window_block_config['type'] ? $window_block_config['type'] : 'register';
        $this->return = $window_block_config['return'] ? $window_block_config['return'] : '';
        $this->show_after_login = $window_block_config['show_after_login'] == 1 ? true : false;

    }

    public function getSelectedSocialsOptions() {
        $socials_options = array(
            '#type' => 'value',
        );
        $socials = array();

        $social_networks = $this->state->get('socials')['socials'];
        foreach($social_networks as $social=>$text) {
            if($text != 0 || $text != "0") {
                $text = strtolower($text);
                $socials[$text] = t($text);
            }
        }
        $socials_options['#value'] = $socials;
        return $socials_options;
    }

}
?>
