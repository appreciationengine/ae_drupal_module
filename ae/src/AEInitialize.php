<?php

namespace Drupal\ae;

use \GuzzleHttp\Exception\RequestException;

class AEInitialize {

    function __construct()
    {
        $this->client = \Drupal::httpClient();
        $this->state = \Drupal::state();
    }

    private $request_status_msgs = array(
        'success' => 'Your framework has been successfully validated',
        'error' => "There was an error initializing your framework. Please ensure that you have properly entered your
        API key and Instance.",
        'whitelist_error' => 'Error whitelisting. Please Ensure that your instance is entered in correctly',
        'whitelist_success' => 'Your domain has been successfully whitelisted.',
        'remote_whitelisting_disabled' =>  'Your AE Plan does not permit for whitelist API Calls. Please sign into your Dashboard and manually whitelist your domain.'
                    . '<strong> If you have already whitelisted your domain, you can ignore this message</strong>'
    );

    /**
     * Modified by: Avery K.
     *
     *
     * @param  [type] $base_url [description]
     * @param  [type] $api_key  [description]
     *
     * @return array    $result_array       a valid configuration and boolean
     *                                      true value if the request succeeded.
     *
     * @return array    $result_array       an error message, and boolean false
     *                                      value if the request threw a RequestException
     */
    public function getConfig($base_url, $api_key) {

        $url = $base_url . '/v1.1/app/info?apiKey=' . $api_key . '&turnoffdebug=1';

        try{

            $request = $this->client->get($url);
            $response = $request->getBody();
            $config = json_decode($response, true); // returns an associative array

            if($this->ae_response_is_valid($config)) {

                $result_array = array($config, true);

            } else {

                $result_array = isset($config['error']) && isset($config['error']['message']) ?
                    array($config['error']['message'], false) :
                    array($this->request_status_msgs['error'], false);

            }

        } catch(RequestException $e) { // request failed
            // $err_code = $e->getResponse()->getStatusCode(); // getResponse seems to return NULL
            $err_msg = $this->request_status_msgs['error'];
            $result_array = array($err_msg, false);

        }

        return $result_array;

    }

    /**
     * @author Avery K.
     * [ae_response_is_valid description]
     * @param  array $config ae framework configuration/AE error response
     * @return Boolean         true if valid response returned from AE, false if error
     */
    public function ae_response_is_valid($config) {

        if(isset($config['error'])) {

            return false;

        } else {

            return true;

        }

    }

    /**
     * [whitelist_domain description]
     * @param  [type] $base_url [description]
     * @param  [type] $api_key  [description]
     *
     * @return array  $result_array   array($status_msg, $status)
     *                                $status_msg is the status of the whitelist, code corresponds to the drupal $status for status messaging
     */
    public function whitelist_domain($base_url, $api_key) {

        $site_url = \Drupal::request()->getSchemeAndHttpHost();
        // Prepare the POST request URL in order to whitelist the client's domain
        $white_list_request = $base_url . '/v1.1/app/whitelist?apiKey=' . $api_key
            . '&turnoffdebug=1' . '&domain=' . $site_url;

        try{

            $request = $this->client->post($white_list_request);
            $response = $request->getBody();
            $decoded_response = json_decode($response, true); // returns an associative array

            if($this->ae_response_is_valid($decoded_response)) { // successful remote whitelist

                $result_array = array($this->request_status_msgs['whitelist_success'], 'status');

            } else { // Unsuccessful remote whitelist

                if( isset($decoded_response['error']) && isset($decoded_response['error']['code']) ) {

                    if($decoded_response['error']['code'] == 12) { // Client's plan doesn't allow remote whitelisting

                        $result_array = array($this->request_status_msgs['remote_whitelisting_disabled'], 'warning');

                    } else {

                        $result_array = array($this->request_status_msgs['error'], 'error'); // use AE's error message

                    }

                } else { // fallback error messaging (if AE's error & message arrays aren't present in isset checks)

                    $result_array = array($this->request_status_msgs['whitelist_error'], 'error');

                }
            }

        } catch(RequestException $e) { // request failed

            $err_msg = $this->request_status_msgs['error'];
            $result_array = array($err_msg, 'error');

        }

        return $result_array;

    }

    /**
     * Getter method for getting the request status messages
     *
     * @return array assoc array of ae status messages
     */
    public function get_request_status_msgs() {

        return $this->request_status_msgs;

    }

    /**
     * Parses a url and returns a sanitized url
     * @param  [type] $url [description]
     * @return [type]      [description]
     */
    public function ae_sanitize_url($url) {
        $components = parse_url($url);
        $scheme = array_key_exists('scheme', $components) ? $components['scheme'] . '://' : 'http://';
        $host = array_key_exists('host', $components) ? $components['host'] : '';
        // if host is empty, it might be set to path
        $host = empty($host) && array_key_exists('path', $components) ? $components['path'] : $host;

        $clean_url = $scheme . $host;
        return $clean_url;
    }

}
