<?php

namespace Drupal\ae\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Serialization\Json;
use Drupal\user\Entity\User;

class LoginController extends ControllerBase {

    /*
        Create a local Drupal user based on the ae user.
        Log in conditionally.
        If the user already exists, use that account instead of creating new one.
        TODO: Drupal allows duplicate emails. We should handle this issue like Wordpress.
    */
    public function createuser($aeid, $createlocal, $signinlocal) {

        $createLocalUser = $createlocal == "true";
        $signinLocalUser = $signinlocal == "true";

        $ae_user = $this->fetch_ae_user($aeid);

        if($this->drupal_user_exists($aeid)) {
            $drupal_user = $this->fetch_drupal_user($aeid);
        }
        else if($this->returning_user_with_different_aeid($ae_user)) {
            $drupal_user = $this->fetch_returning_user($ae_user);
        }
        else if( $drupal_user_with_vemail = $this->returning_user_with_matching_verified_email($ae_user) ) {
            if(!empty($drupal_user_with_vemail)) {
                $this->update_ae_drupal_userdata($ae_user, $drupal_user_with_vemail);
                $drupal_user = $drupal_user_with_vemail;
            }
        }
        else {
            if($createLocalUser) {
                $drupal_user = $this->create_new_drupal_user($ae_user);
                $this->create_local_ae_user($drupal_user, $ae_user);
            }
        }
        $this->add_services_for_user($drupal_user, $ae_user);

        if($signinLocalUser)
            $this->sign_in_local_drupal_user($drupal_user);

        exit(0);
    }

    /**
     * @todo get rid of hardcoded url !!!!
     *
     * @param  [type] $aeid [description]
     * @return [type]       [description]
     */
    private function fetch_ae_user($aeid) {
        $state = \Drupal::state();
        $client = \Drupal::httpClient();
        $instance_url = $state->get('base_url');
        $ts = time();

        $api_key = $state->get('api_key');
        $instance_url = rtrim($instance_url, '/');
        $url = $instance_url . '/v1.1/member/' . $aeid . "?apiKey=" . $api_key . "&ts=" . $ts;

        $request = $client->get($url);
        $ae_user = $request->getBody();
        $user_json = json_decode($ae_user);
        return $user_json;
    }

    private function drupal_user_exists($aeid) {
        $uid = $this->fetch_uid_from_aeid($aeid);
        $account = \Drupal\user\Entity\User::load($uid);
        $empty = !empty($uid);
        return !empty($uid);
        // return is_numeric($uid);
    }

    private function fetch_drupal_user($aeid) {
        $uid = $this->fetch_uid_from_aeid($aeid);
        $drupal_user = \Drupal\user\Entity\User::load($uid);
        $drupal_user->activate();
        $drupal_user->save();
        return $drupal_user;
    }

    private function returning_user_with_different_aeid($ae_user) {

        foreach ($ae_user->services as $service) {
            $aeid = db_query("SELECT aeid FROM {ae_services} WHERE serviceID = :sid", [':sid' => $service->ID])->fetchField();
            if(is_numeric($aeid)) {
                return true;
            }
        }
        return false;

    }

    private function returning_user_with_matching_verified_email($ae_user) {
        $ae_user_verified_email = $ae_user->data->VerifiedEmail;
        $onrecord_drupal_user = $this->fetch_drupal_user_by_verified_email($ae_user_verified_email);
        return $onrecord_drupal_user;
    }

    private function fetch_returning_user($ae_user) {
        foreach ($ae_user->services as $service) {
            $aeid = db_query("SELECT aeid FROM {ae_services} WHERE serviceID = :sid", [':sid' => $service->ID])->fetchField();
            if(isset($aeid)) {
                $drupal_user = $this->fetch_drupal_user($aeid);
                $this->update_ae_drupal_userdata($ae_user, $drupal_user);
                $drupal_user->activate();
                $drupal_user->save();
                return $drupal_user;
            }
        }
    }

    private function create_new_drupal_user($ae_user) {
        $user_name = $this->get_unique_username_from_name($ae_user->data->Username);
        $rand_pass = $this->generateRandomPassword();
        $drupal_user = User::create();
        $drupal_user->setPassword($rand_pass);
        $drupal_user->enforceIsNew();
        $drupal_user->setEmail($ae_user->data->Email);
        $drupal_user->setUsername($user_name);
        $drupal_user->activate();
        $drupal_user->save();
        return $drupal_user;
    }

    private function get_unique_username_from_name($name) {
        $unique = false;
        $i = 1;
        while(!$unique) {
            $uid = $this->get_uid_by_username($name);
            if(!is_numeric($uid)) {
                $unique = true;
            }
            else {
                $name .= $i;
                $i++;
            }
        }
        return $name;
    }

    private function sign_in_local_drupal_user($drupal_user) {
        user_login_finalize($drupal_user);
    }

    private function create_local_ae_user($drupal_user, $ae_user) {
        $uid = $drupal_user->id();

        db_insert('ae_users')->fields([
            'aeid' => isset($ae_user->data->ID) ? $ae_user->data->ID: "",
            'uid' => $uid,
            'FirstName' => isset($ae_user->data->FirstName) ? $ae_user->data->FirstName: "",
            'SurName' => isset($ae_user->data->SurName) ? $ae_user->data->SurName : "",
            'Email' => isset($ae_user->data->Email) ? $ae_user->data->Email : "",
            'City' => isset($ae_user->data->City) ? $ae_user->data->City : "",
            'State' => isset($ae_user->data->State) ? $ae_user->data->State : "",
            'Country' => isset($ae_user->data->Country) ? $ae_user->data->Country : "",
            'Postcode' => isset($ae_user->data->PostCode) ? $ae_user->data->PostCode : "",
            'MobilePhone' => isset($ae_user->data->MobilePhone) ? $ae_user->data->MobilePhone : "",
            'Website' => isset($ae_user->data->Website) ? $ae_user->data->Website : "",
            'Username' => isset($ae_user->data->Username) ? $ae_user->data->Username : "",
            'VerifiedEmail' => isset($ae_user->data->VerifiedEmail) ? $ae_user->data->VerifiedEmail : "",
            'Bio' => isset($ae_user->data->Bio) ? $ae_user->data->Bio : "",
            'Gender' => isset($ae_user->data->Gender) ? $ae_user->data->Gender : "",
            'BirthDate' => isset($ae_user->data->BirthDate) ? $ae_user->data->BirthDate : "",
        ])->execute();
    }

    private function add_services_for_user($drupal_user, $ae_user) {
        $aeid = $ae_user->data->ID;
        // for each service for the ae user
        foreach ($ae_user->services as $service) {

            if(!$this->service_exists($service)) {
                db_insert('ae_services')->fields([
                    'serviceID' => isset($service->ID) ? $service->ID : "",
                    'aeid' => isset($ae_user->data->ID) ? $ae_user->data->ID : ""
                    //$drupal_user->id()
                ])->execute();
            }
            else if(!$this->service_exists_on_aeid($aeid)) { // service exists, but does not exist on the aeUser being processed
                db_update('ae_services')
                    ->fields(array(
                        'aeid' => isset($ae_user->data->ID) ? $ae_user->data->ID : ""
                    ))
                    ->condition('serviceID', $service->ID, '=')
                    ->execute();
            }
        }

    }

    private function service_exists_on_aeid($id) {
        $service_id = db_query("SELECT serviceID FROM ae_services WHERE aeid = :id LIMIT 1;", [':id' => $id])->fetchField();
        return is_numeric($service_id);
    }

    private function update_ae_drupal_userdata($ae_user, $drupal_user) {
        $uid = $drupal_user->id();
        $num_updated = db_update('ae_users')
          ->fields(array(
              'aeid' => isset($ae_user->data->ID) ? $ae_user->data->ID: "",
              'uid' => $uid,
              'FirstName' => isset($ae_user->data->FirstName) ? $ae_user->data->FirstName: "",
              'SurName' => isset($ae_user->data->SurName) ? $ae_user->data->SurName : "",
              'Email' => isset($ae_user->data->Email) ? $ae_user->data->Email : "",
              'City' => isset($ae_user->data->City) ? $ae_user->data->City : "",
              'State' => isset($ae_user->data->State) ? $ae_user->data->State : "",
              'Country' => isset($ae_user->data->Country) ? $ae_user->data->Country : "",
              'Postcode' => isset($ae_user->data->PostCode) ? $ae_user->data->PostCode : "",
              'MobilePhone' => isset($ae_user->data->MobilePhone) ? $ae_user->data->MobilePhone : "",
              'Website' => isset($ae_user->data->Website) ? $ae_user->data->Website : "",
              'Username' => isset($ae_user->data->Username) ? $ae_user->data->Username : "",
              'VerifiedEmail' => isset($ae_user->data->VerifiedEmail) ? $ae_user->data->VerifiedEmail : "",
              'Bio' => isset($ae_user->data->Bio) ? $ae_user->data->Bio : "",
              'Gender' => isset($ae_user->data->Gender) ? $ae_user->data->Gender : "",
              'BirthDate' => isset($ae_user->data->BirthDate) ? $ae_user->data->BirthDate : "",
          ))
          ->condition('uid', $uid, '=')
          ->execute();
    }

    private function service_exists($service) {
        $service_id = db_query("SELECT serviceID FROM ae_services WHERE serviceID = :serviceID LIMIT 1;", [':serviceID' => $service->ID])->fetchField();
        return is_numeric($service_id);
    }

    private function fetch_uid_from_aeid($aeid) {
        $uid = db_query("SELECT uid FROM {ae_users} WHERE aeid = :aeid", [':aeid' => $aeid])->fetchField();
        return $uid;
    }


    public function say() {
        return array(
            '#type' => 'markup',
            '#markup' => $this->t('Hello Akshay'),
        );
    }

    private function fetch_drupal_user_by_verified_email($VerifiedEmail) {

        if(!empty($VerifiedEmail)) {
            $drupal_user = NULL;
            $drupal_uid = db_query(
                "SELECT uid FROM {ae_users} WHERE VerifiedEmail = :vemail", [':vemail' => $VerifiedEmail]
            )
            ->fetchField();
            if(is_numeric($drupal_uid)) {
                $drupal_user = \Drupal\user\Entity\User::load($drupal_uid);
            }
            return $drupal_user;

        } else {
            return NULL;
        }

    }

    private function get_uid_by_username($username) {
        $uid = db_query("SELECT uid FROM {users_field_data} WHERE name = :name", [':name' => $username])->fetchField();
        return $uid;
    }

    /**
     * Code used from https://stackoverflow.com/questions/1837432/how-to-generate-random-password-with-php
     * @return [type] [description]
     */
    private function generateRandomPassword() {
        $password = '';

        $desired_length = rand(8, 12);

        for($length = 0; $length < $desired_length; $length++) {
        //Append a random ASCII character (including symbols)
        $password .= chr(rand(32, 126));
        }

        return $password;
    }

}


?>
