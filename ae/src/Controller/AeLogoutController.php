<?php

namespace Drupal\ae\Controller;

use \Drupal\user\Controller\UserController;
use \Drupal\Core\Routing\TrustedRedirectResponse;

class AeLogoutController extends UserController {

    public function get_state() {
        return \Drupal::state();
    }

    public function logout() {

        $state = $this->get_state();
        $home_url = $GLOBALS['base_url'];
        $logout_link = $state->get('ae_logout_url');

        $return_url = '?auth_method=direct&return_url=' . $home_url;
        $redirect_url = $logout_link . $return_url;

        user_logout();
        session_write_close();

        $redirect_url = urlencode($redirect_url);
        return $this->redirect('ae.redirect', array('url' => $redirect_url));
    }

}
