<?php

namespace Drupal\ae\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Drupal\Core\Routing\TrustedRedirectResponse;

class RedirectOffsite extends ControllerBase {

    public function redirect_to() {
        $url = $_REQUEST['url'];
        $url = urldecode($url);
        return new TrustedRedirectResponse($url);
    }

}
