<?php
/**
 * @author Avery K.
 * [namespace description]
 * @var [type]
 */

namespace Drupal\ae\Form\General;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
// use Symfony\Component\HttpFoundation\Request; // Avery

class OptInsForm extends ConfigFormBase {

    protected function getEditableConfigNames() {
        return [
            'ae.optInsForm' // defined in ae.links.task.yml
        ];
    }

    public function getFormId() {
        return 'ae_opt_ins_form';
    }

    function __construct() {

        $this->state = \Drupal::state();

    }

    public function buildForm(array $form, FormStateInterface $form_state) {

        $checked = array(
            'checked' => t('Enable By Default'),

        );

        $opt_in_data = $this->state->get('opt_ins');
        if(isset($opts["options"])) {
            $default_values = array_keys(array_filter($opt_in_data)["options"]);
        }
        else {
            $default_values = [];
        }

        // Optins
        $form['opt_ins'] = array(
            '#type' => 'textarea',
            '#title' => t('Opt Ins'),
            '#rows' => 20,
            '#default_value' => $opt_in_data['opt_ins']
        );

        return parent::buildForm($form, $form_state);
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

        parent::submitForm($form, $form_state);

        $form_state->cleanValues();

        $this->state->set('opt_ins', $form_state->getValues());

        ksm($this->state->get('opt_ins'));

    }


}
