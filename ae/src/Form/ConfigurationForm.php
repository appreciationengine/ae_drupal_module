<?php

namespace Drupal\ae\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ae\AEInitialize;

class ConfigurationForm extends ConfigFormBase {

    private $default_instance = 'https://theappreciationengine.com';

    protected function getEditableConfigNames() {
        return [
            'ae.adminsettings'
        ];
    }

    public function getFormId() {
        return 'ae_config_form';
    }

    function __construct()
    {
        $this->client = \Drupal::httpClient();
        $this->state = \Drupal::state();
    }

    public function buildForm(array $form, FormStateInterface $form_state) {

        $default_instance = $this->default_instance;
        $instance_url = $this->state->get('base_url');
        $instance_url = $this->ae_sanitize_url($instance_url);

        $form['base_url'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Instance <i>(Leave blank for the default: ' . $default_instance . ')</i>'),
            '#default_value' => !empty( $instance_url ) ? $instance_url : $default_instance,
            '#placeholder' => $default_instance,
        ];

        $form['api_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('API Key'),
            '#default_value' => $this->state->get('api_key')
        ];

        return parent::buildForm($form, $form_state);
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

        $api_key = $form_state->getValue('api_key');
        $base_url = $form_state->getValue('base_url');

        $Initializer = new AEInitialize();

        $result = $Initializer->getConfig($base_url, $api_key);
        $status_msgs = $Initializer->get_request_status_msgs();
        ksm($result);
        if($result[1]) {

            $config = $result[0];
            parent::submitForm($form, $form_state);
            drupal_set_message(t($status_msgs['success']), 'status');
            $this->do_whitelist($Initializer, $base_url, $api_key);

            $this->state->set('api_key', $api_key);
            $this->state->set('config', $config);
            $this->state->set('base_url', $base_url);
            $this->state->set('framework_url', $config['Widget']['URL']);
            $this->state->set('app_name', $config['Slug']);
            $this->state->set('ae_logout_url', $config['LogoutURL']);

        } else {

            $err_msg = $result[0];
            drupal_set_message(t($err_msg), 'error');

        }

    }

    /**
     * @author Avery K.
     *
     * triggers whitelist and handles status messaging
     */
    private function do_whitelist($Initializer, $base_url, $api_key) {

        $wl_data = $Initializer->whitelist_domain($base_url, $api_key);
        if($wl_data[1]) {

            drupal_set_message(t($wl_data[0]), 'status');

        } else {

            drupal_set_message(t($wl_data[0]), 'error');

        }

    }

    /**
     * Parses a url and returns a sanitized url
     * @param  [type] $url [description]
     * @return [type]      [description]
     */
    private function ae_sanitize_url($url) {
        $components = parse_url($url);
        $scheme = array_key_exists('scheme', $components) ? $components['scheme'] . '://' : 'http://';
        $host = array_key_exists('host', $components) ? $components['host'] : '';
        // if host is empty, it might be set to path
        $host = empty($host) && array_key_exists('path', $components) ? $components['path'] : $host;

        $clean_url = $scheme . $host;
        return $clean_url;
    }

}


?>
